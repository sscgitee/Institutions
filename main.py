"""
Author: Mr.Sun
Datetime: 2023/5/12 17:31 
FileName: main.py
Desc: 
"""
import time
from scrapy.crawler import CrawlerProcess
from Institutions.spiders import JobInformation
from Institutions.utils.logger import logger

start_time = time.time()

# 创建爬虫进程对象
process = CrawlerProcess()

# 启动爬虫
process.crawl(JobInformation.JobinformationSpider, page=10)
process.start()  # 启动爬虫流程

end_time = time.time()

# 输出爬虫执行时间
logger.info("Spider took %s seconds" % (end_time - start_time))
