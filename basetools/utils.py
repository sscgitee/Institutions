"""
Author : Mr.Sun
Datetime : 2023/10/20 09:32 
FileName : utils.py
Desc : 处理各种路径
"""

import os
import dataset
from sqlalchemy import create_engine

# 项目根目录
project_dir = os.path.dirname(os.path.abspath(__file__))
# 数据库地址
db_address = os.path.join(project_dir, 'data.db')
# excel 地址
excel_address = os.path.join(project_dir, '2024.xls')

# 创建数据库
db = dataset.connect('sqlite:///{}'.format(db_address),
                     engine_kwargs={'connect_args': {'check_same_thread': True}},
                     sqlite_wal_mode=False)

engine = create_engine('sqlite:///{}'.format(db_address), echo=True)
