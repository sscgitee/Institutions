"""
Author : Mr.Sun
Datetime : 2023/10/20 09:29 
FileName : sift_posts.py
Desc :  职位筛选 ,写入csv

sql 语句筛选
pandas 直接读取并筛选
"""
import pandas as pd
from utils import db, project_dir, os, excel_address


# 使用sql 直接筛选
def sql_result_myself(table):
    try:
        sql = '''select * from (select *
          from {table_name}
          where 基层工作最低年限 in ('无限制')
            and 服务基层项目工作经历 in ('无限制')
            and 学历 in ('仅限本科', '大专及以上', '大专或本科', '本科及以上', '本科或硕士研究生')
            and 专业 like '%计算机类%'
          union
          select *
          from {table_name}
          where 基层工作最低年限 in ('无限制')
            and 服务基层项目工作经历 in ('无限制')
            and 学历 in ('仅限本科', '大专及以上', '大专或本科', '本科及以上', '本科或硕士研究生')
            and 专业 like '%网络工程%')
    where 备注 not like '%应届高校毕业生%'
      and 备注 not like '%2024届高校毕业生%'
      and 备注 not like '%英语四级%'
      and 备注 not like '%英语六级%'
      and 备注 not like '%女性%'
      and 备注 not like '%律师从业资格或通过国家司法考试%'
      and 备注 not like '%生源%'
      and 备注 not like '%在办税服务厅工作不少于3年%';'''.format(table_name=table)
        result = [x for x in db.query(sql)]
        dataframe = pd.DataFrame(result)
        file_name = os.path.join(project_dir, table + ".csv")
        dataframe.to_csv(file_name, index=False, sep=',')
    except Exception as e:
        print(e)


# 直接读excel 使用pandas 过滤数据
def pandas_result_myself(sheet_name):
    # 读取excel 的数据并过滤
    df = pd.read_excel(excel_address, sheet_name=sheet_name)

    print(df)
