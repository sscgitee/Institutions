"""
Author : Mr.Sun
Datetime : 2023/10/18 09:51 
FileName : posts_to_db.py
Desc :  将excel 中的数据洗到数据库中
再把筛选结果写回csv 中
"""

import pandas as pd
from utils import engine, excel_address


def excel_to_sqlite(sheet_name):
    try:
        df = pd.read_excel(excel_address, sheet_name=sheet_name)
        # 去除sheet_name 中的特殊字符,创建表名
        sheet_name = sheet_name.replace(" ", "").replace("（", "").replace("）", "")
        # 将数据写到sqlite中
        df.to_sql(sheet_name, engine, if_exists="replace", index=False)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    # 获取excel 中的sheet_name
    excel = pd.ExcelFile(excel_address)
    sheet_names = excel.sheet_names
    for temp in sheet_names:
        excel_to_sqlite(temp)
