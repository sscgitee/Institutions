"""
Author: Mr.Sun
Datetime: 2023/11/25 15:37
FileName: teacher_use.py
Desc:


#include <stdio.h>

int main() {
   double h = 100; // 使用除法会出现小数，所以用浮点型
   double s = 100;
   h = h / 2; // 计算第一次反弹高度
   for (int i = 2; i <= 10; i++) {
      s = s + 2 * h; // 下落和反弹算两次，所以要乘2
      h = h / 2;
   }
   printf("第10次落地时，共经过%f米，第10次反弹高%f米\n", s, h);
   return 0;
}

"""

# 2023 年小球回弹经典问题
def main():
    h = 56
    s = 56
    h = h / 2
    for i in range(2, 5):
        s = s + 2 * h
        h = h / 2
    print(f"第四次落地时，共经过了{s}米,反弹高度为{h}米")


if __name__ == '__main__':
    main()
