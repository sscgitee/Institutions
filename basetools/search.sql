#
查询武北南
select *
from (select *
      from 中央国家行政机关参照公务员法管理事业单位
      where 基层工作最低年限 in ('无限制')
        and 服务基层项目工作经历 in ('无限制')
        and 学历 in ('仅限本科', '大专及以上', '大专或本科', '本科及以上', '本科或硕士研究生')
        and 专业 like '%计算机类%'
        and 政治面貌 in ('不限')
        and 工作地点 like '%黑龙江%'
      union
      select *
      from 中央国家行政机关参照公务员法管理事业单位
      where 基层工作最低年限 in ('无限制')
        and 服务基层项目工作经历 in ('无限制')
        and 学历 in ('仅限本科', '大专及以上', '大专或本科', '本科及以上', '本科或硕士研究生')
        and 专业 like '%网络工程%'
        and 政治面貌 in ('不限')
        and 工作地点 like '%黑龙江%')
where 备注 not like '%应届高校毕业生%'
  and 备注 not like '%2024届高校毕业生%'
  and 备注 not like '%英语四级%'
  and 备注 not like '%英语六级%'
  and 备注 not like '%女性%'
  and 备注 not like '%律师从业资格或通过国家司法考试%'
  and 备注 not like '%生源%'
  and 备注 not like '%在办税服务厅工作不少于3年%';

#查询自己
select *
from (select *
      from 中央国家行政机关参照公务员法管理事业单位
      where 基层工作最低年限 in ('无限制')
        and 服务基层项目工作经历 in ('无限制')
        and 学历 in ('仅限本科', '大专及以上', '大专或本科', '本科及以上', '本科或硕士研究生')
        and 专业 like '%计算机类%'
      union
      select *
      from 中央国家行政机关参照公务员法管理事业单位
      where 基层工作最低年限 in ('无限制')
        and 服务基层项目工作经历 in ('无限制')
        and 学历 in ('仅限本科', '大专及以上', '大专或本科', '本科及以上', '本科或硕士研究生')
        and 专业 like '%网络工程%')
where 备注 not like '%应届高校毕业生%'
  and 备注 not like '%2024届高校毕业生%'
  and 备注 not like '%英语四级%'
  and 备注 not like '%英语六级%'
  and 备注 not like '%女性%'
  and 备注 not like '%律师从业资格或通过国家司法考试%'
  and 备注 not like '%生源%'
  and 备注 not like '%在办税服务厅工作不少于3年%';

select *
from 中央党群机关
where 基层工作最低年限 in ('无限制')
    and 服务基层项目工作经历 in ('无限制')
    and 专业 like '%计算机类%'
   or 专业 like '%网络工程%';

select *
from 中央国家行政机关（本级）
where 基层工作最低年限 in ('无限制') and 服务基层项目工作经历 in ('无限制')
    and 专业 like '%计算机类%'
   or 专业 like '%网络工程%';

select 学历
from 中央国家行政机关参照公务员法管理事业单位
group by 学历;
