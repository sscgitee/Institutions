-- 关键词
create table key_words
(
    id          integer not null
        constraint key_words_pk
            primary key autoincrement,
    key_word    varchar(255),        -- 关键词
    priority    integer   default 1, -- 优先级
    status      integer   default 1, -- 状态
    create_time timestamp default (datetime('now', 'localtime'))
);

insert into key_words(key_word, priority)
values ('北京市', 1),
       ('天津市', 1),
       ('青岛市', 1),
       ('南京市', 1),
       ('无锡市', 1),
       ('苏州市', 1),
       ('沈阳市', 1),
       ('伊春市', 1),
       ('哈尔滨市', 1),
       ('合肥市', 1),
       ('阜阳市', 1),
       ('黄山市', 1),
       ('阿克苏地区', 1),
       ('喀什地区', 1),
       ('安徽省', 2),
       ('新疆维吾尔自治区', 2),
       ('黑龙江省', 2),
       ('吉林省', 2),
       ('湖南省', 2),
       ('四川省', 2)
-- auto-generated definition
create table job_information
(
    id          integer not null
        constraint job_information_pk
            primary key autoincrement,
    exam_type   varchar(255),
    url         varchar not null,
    title       text,
    key_word    varchar,
    publish_at  varchar(255),
    create_time timestamp default (datetime('now', 'localtime'))
);

