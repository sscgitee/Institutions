## 本地创建项目与远程仓库链接

1. 在本地创建项目

```
 pip install Scrapy 
 scrapy startproject Institutions 

```

2. 关联远程仓库

```
git  init 
git remote add origin https://gitee.com/sscgitee/Institutions.git
git pull origin master
git branch --set-upstream-to=origin/master main
git add .
git push origin HEAD:master
git checkout master
```

3. 开始爬虫

```
创建一个爬虫
scrapy genspider JobInformation "https://www.chinagwy.org/html/zkgg/index.html"

scrapy crawl JobInformation   -a page=页数
需要在爬虫文件新增接收参数
```

4. 多个爬虫

```
pipeline 增加数据处理的类
settings.py 里面增加新的ITEM_PIPELINES

```

5. 一些工具类文件
6. 注意点

```
1. 有些网站的编码是gb2312，需要在settings.py里面增加
2. dataset 链接sqlite 默认开启了wal 模式，写入数据后，需要关闭wal模式，才能读取到数据，所以直接在创建的时候关闭wal模式 具体如下
 db = dataset.connect('sqlite:///{}'.format(base_dir),
                    engine_kwargs={'connect_args': {'check_same_thread': True}},
                      sqlite_wal_mode=False)
```