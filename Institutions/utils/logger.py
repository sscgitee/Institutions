"""
Author： Mr.Sun
Datetime： 2023/5/10 10:50 
FileName: logger.py
Desc:  记录日志的文件
"""
import os
import json
import time
import logging
import logging.handlers
from .data_operate import project_dir


def singleton(cls):
    instances = {}

    def wrapper(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return wrapper


@singleton
class Log(object):
    """
    logging的初始化操作，以类封装的形式进行
    """

    def __init__(self):
        # 定义对应的程序模块名name，默认为root
        self.logger = logging.getLogger()

        # 设置输出的等级
        level_relations = {'noset': logging.NOTSET,
                           'debug': logging.DEBUG,
                           'info': logging.INFO,
                           'waring': logging.WARNING,
                           'error': logging.ERROR,
                           'critical': logging.CRITICAL}

        # log_path是存放日志的路径
        times_str = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        lib_path = os.path.abspath(os.path.join(project_dir, 'logs'))

        # 如果不存在这个logs文件夹，就自动创建一个
        if not os.path.exists(lib_path):
            os.mkdir(lib_path)
        # 日志文件的地址
        self.log_name = os.path.join(lib_path , 'scrapy_institutions.log')

        # 必须设置，这里如果不显示设置，默认过滤掉warning之前的所有级别的信息
        self.logger.setLevel(level_relations.get('info'))

        # 日志输出格式
        msg = {
            "time": "[%(asctime)s]",
            "level": "[%(levelname)s]",
            "file": "[%(filename)s]",
            "message": "%(message)s"
        }
        self.formatter = logging.Formatter(json.dumps(msg, ensure_ascii=False))
        # 先
        # if not self.logger.handlers:
        # 创建一个FileHandler， 向文件logname输出日志信息
        file_handle = logging.handlers.RotatingFileHandler(filename=self.log_name,
                                                           maxBytes=1024 * 1024 * 5,
                                                           backupCount=3,
                                                           encoding='utf-8')
        # 设置日志等级
        file_handle.setLevel(logging.INFO)
        # 设置handler的格式对象
        file_handle.setFormatter(self.formatter)
        # 将handler增加到logger中
        self.logger.addHandler(file_handle)

        # 创建一个StreamHandler,用于输出到控制台
        # console_handle = logging.StreamHandler()
        # console_handle.setLevel(logging.INFO)
        # console_handle.setFormatter(self.formatter)
        # self.logger.addHandler(console_handle)

        # 关闭打开的文件
        file_handle.close()


logger = Log().logger
