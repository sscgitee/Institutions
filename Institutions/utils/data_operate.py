"""
Author： Mr.Sun
Datetime： 2023/5/10 10:14 
FileName: data_operate.py
Desc:  数据库操作
"""

import os

# import dataset

project_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
base_dir = os.path.join(project_dir, 'data.db')

# windows 的
# base_dir = os.path.join(project_dir, 'windows.db')


# db = dataset.connect('sqlite:///{}'.format(base_dir),
#                      engine_kwargs={'connect_args': {'check_same_thread': True}},
#                      sqlite_wal_mode=False)
# print(db.tables)
