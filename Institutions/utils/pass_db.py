"""
Author : Mr.Sun
Datetime : 2024/7/9 11:29 
FileName : pass_db.py
Desc :  这种不支持密码
"""
import os
import dataset
from sqlalchemy import event
from sqlalchemy.engine import Engine

project_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
base_dir = os.path.join(project_dir, 'test_passwd.db')
password = "12345"

# 创建数据库连接
db = dataset.connect(f"sqlite:///{base_dir}",
                     engine_kwargs={'connect_args': {'check_same_thread': False}}
                     , sqlite_wal_mode=False)


# 确保在每次连接时设置密码
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute(f"PRAGMA key='{password}'")
    cursor.close()


print(db.tables)
# 示例：创建表并插入数据
table = db['test_table']
table.insert(dict(name='Alice', age=30))

# 查询数据
print(list(table.all()))