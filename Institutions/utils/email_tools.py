"""
Author: Mr.Sun
Datetime: 2023/5/12 14:00 
FileName: email_tools.py
Desc: 
"""
import yagmail

from decorators import except_decorator


@except_decorator
def send_email():
    # 设置发件人邮箱和密码
    sender_email = 'shicheng_sun@qingsongchou.com'
    sender_password = 'Ssc@950118'

    # 邮件正文内容
    html = "<html><body><h1>Hello World!</h1><p>This is a demo email sent using yagmail.</p></body></html>"

    # 创建yagmail对象并发送邮件
    yag = yagmail.SMTP(sender_email, sender_password, host='smtp.exmail.qq.com')
    yag.send(
        to='m18226287291@163.com',
        subject='HTML demo',
        contents=html)
