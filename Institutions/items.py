# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class InstitutionsItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class JobInformationItem(scrapy.Item):
    exam_type = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    key_word = scrapy.Field()
    publish_at = scrapy.Field()
