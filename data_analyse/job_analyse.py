"""
Author: Mr.Sun
Datetime: 2023/5/26 13:45 
FileName: job_analyse.py
Desc:  职位数据分析
"""
import dataset
import pandas as pd
import matplotlib.pyplot as plt
from Institutions.utils.data_operate import base_dir
from matplotlib.font_manager import FontProperties

# font = FontProperties(fname='GB2312.ttf', size=14)

db = dataset.connect('sqlite:///{}'.format(base_dir),
                     engine_kwargs={'connect_args': {'check_same_thread': True}},
                     sqlite_wal_mode=False)

# 1.统计各个城市的职位数量
city_job_nums = 'select key_word, count(*) as num from job_information group by key_word ;'
result = db.query(city_job_nums)

data = [{"city": x['key_word'], "num": x['num']} for x in db.query(city_job_nums)]
print(data)

# 将您提供的数据生成两个列表，一个存放城市名称，一个存放数量
# cities = []
# numbers = []
# for item in data:
#     cities.append(item['city'])
#     numbers.append(item['num'])
#
# # 绘制直方图
# plt.hist(numbers, bins=10, color='blue', alpha=0.5)
# plt.xlabel('Population', fontproperties=font)
# plt.ylabel('City Number', fontproperties=font)
# plt.title('City Population Distribution', fontproperties=font)
# plt.xticks(range(0, max(numbers) + 10, 10))
# for i in range(len(numbers)):
#     text = cities[i] + ': ' + str(numbers[i])
#     plt.text(numbers[i], i, text, ha='left', va='center')
# plt.show()

# # 字体设置
# font = FontProperties(fname='GB2312.ttf', size=14)
#
# # 数据准备
#
# # 将数据转换为数组
# nums = [d['num'] for d in data]
#
# # 设置直方图的组数
# bins = max(nums) // 20
#
# # 绘图
# fig, ax = plt.subplots(figsize=(8, 6))
# ax.hist(nums, bins=bins, rwidth=0.9)
#
# # 设置标签和字体
# ax.set_xlabel('城市人口数', fontproperties=font)
# ax.set_ylabel('城市数', fontproperties=font)
# ax.set_title('城市人口数直方图', fontproperties=font)
#
# plt.show()


# import seaborn as sns
# import matplotlib.pyplot as plt
# import matplotlib.font_manager as fm
#
# # 导入中文字体
# font = fm.FontProperties(fname='GB2312.ttf', size=8)
#
# # 定义 x 轴和 y 轴的数据
# x = [item['city'] for item in data]
# y = [item['num'] for item in data]
#
# # 绘制直方图
# sns.barplot(x=x, y=y)
#
# # 设置 x 轴标签文字和 x 轴标签旋转角度
# plt.xticks(rotation=60, fontproperties=font)
#
# # 设置 y 轴标签文字
# plt.ylabel('nums', fontproperties=font)
#
# # 设置标题文字
# plt.title('省市职位展示', fontproperties=font)
#
# # 显示图形
# plt.show()


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm


myfont = fm.FontProperties(fname='GB2312.ttf', size=8)

# 设置样式和字体
sns.set_style("darkgrid")
sns.set(font=myfont.get_name())

# 模拟数据
df = pd.DataFrame(data)

# 设置中文字体
# sns.set(font='GB2312.ttf')

# 绘制图形
plt.figure(figsize=(12, 6))
ax = sns.barplot(x='city', y='num', data=df, palette='Blues_d')
sns.despine()
ax.set_title('城市数量')
ax.set_xlabel('城市')
ax.set_ylabel('数量')
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, horizontalalignment='right')
# 在直方图顶部显示数字
for p in ax.patches:
    ax.annotate(str(p.get_height()), (p.get_x() + p.get_width() / 2, p.get_height()), ha='center', va='bottom')
plt.tight_layout()
plt.show()
